package com.blaskoasky.challangegold;

/*iriantomauduta
06 April'22
Binar Academy*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallangeGoldApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallangeGoldApplication.class, args);
    }
}
