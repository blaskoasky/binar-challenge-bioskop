package com.blaskoasky.challangegold.controller;

import com.blaskoasky.challangegold.mapper.film.AddFilmRequestDTO;
import com.blaskoasky.challangegold.mapper.film.FilmMapper;
import com.blaskoasky.challangegold.mapper.film.UpdateFilmRequestDTO;
import com.blaskoasky.challangegold.model.film.FilmEntity;
import com.blaskoasky.challangegold.model.film.IFilmScheduleDTO;
import com.blaskoasky.challangegold.model.film.ScheduleEntity;
import com.blaskoasky.challangegold.service.film.IFilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bioskop/films")
public class FilmController {

    @Autowired
    private IFilmService filmService;

    @Autowired
    private FilmMapper filmMapper;

    private static final Logger LOG = LoggerFactory.getLogger(FilmController.class);

    //          ----> GET MAPPING <----
    @Operation(summary = "counting films in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Counted!",
                    content = {@Content(schema = @Schema(example = "Counted!"))})
    })
    @GetMapping("/public/count-films")
    public ResponseEntity<Integer> countFilms() {
        Integer jumlahFilms = filmService.countFilms();
        LOG.info("Jumlah Film: {}", jumlahFilms);
        return ResponseEntity.ok(jumlahFilms);
    }

    @Operation(summary = "get a film by filmId, require known filmId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Film shown!",
                    content = {@Content(schema = @Schema(example = "Film shown!"))})
    })
    @GetMapping("/public/show-films")
    public ResponseEntity<List<FilmEntity>> showAllSedangTayang() {
        List<FilmEntity> allFilms = filmService.showAllFilms();
        LOG.info("Films Shown");
        return ResponseEntity.ok(allFilms);
    }

    @Operation(summary = "get a film schedule by filmId, require known filmId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schedule Film shown!",
                    content = {@Content(schema = @Schema(example = "Schedule Film shown!"))})
    })
    @GetMapping("/public/show-schedule-film/{id}")
    public ResponseEntity<List<IFilmScheduleDTO>> showScheduleFilm(@PathVariable("id") Integer filmId) {
        List<IFilmScheduleDTO> listSchd = filmService.showScheduleFilmNew(filmId);
        LOG.info("Schedule Film shown");
        return ResponseEntity.ok(listSchd);
    }

    @Operation(summary = "get a schedule by filmId, require known filmId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schedule Film shown!",
                    content = {@Content(schema = @Schema(example = "Schedule Film shown!"))})
    })
    @GetMapping("/public/get-schedule/{id}")
    public ResponseEntity<List<ScheduleEntity>> showSchedule(@PathVariable("id") Integer filmId) {
        FilmEntity film = new FilmEntity(filmId);
        List<ScheduleEntity> listSchd = filmService.scheduleFilm(film.getFilmId());
        LOG.info("Schedule Film shown");
        return ResponseEntity.ok(listSchd);
    }


    //          ----> POST MAPPING <----
    @Operation(summary = "Add a film, add 'filmCode', 'filmName', and 'sedangTayang' and program will assign the new filmId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Film Added!",
                    content = {@Content(schema = @Schema(example = "Film Added!"))})
    })
    @PostMapping("/admin/add-film")
    public ResponseEntity<HttpStatus> addFilm(@RequestBody AddFilmRequestDTO newFilm) {
        filmService.addFilm(filmMapper.mapAddFilm(newFilm));
        LOG.info("film added: {}", newFilm.getFilmName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    //          ----> UPDATE MAPPING <----
    @Operation(summary = "Change a film data, update 'filmCode', 'filmName', and 'sedangTayang' related to their filmId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Film updated!",
                    content = {@Content(schema = @Schema(example = "Film updated!"))})
    })
    @PutMapping("/admin/update-film")
    public ResponseEntity<HttpStatus> updateFilm(@RequestBody UpdateFilmRequestDTO updatedFilm) {
        filmService.updateFilm(filmMapper.mapUpdateFilm(updatedFilm));
        LOG.info("film updated: {}", updatedFilm.getFilmName());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //          ----> DELETE MAPPING <----
    @Operation(summary = "Delete a film and their related schedule entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Film deleted!",
                    content = {@Content(schema = @Schema(example = "Film deleted!"))})
    })
    @DeleteMapping("/admin/delete-film/{id}")
    public ResponseEntity<HttpStatus> deleteFilm(@PathVariable("id") Integer filmId) {
        filmService.deleteScheduleFilm(filmId);
        filmService.deleteFilm(filmId);
        LOG.info("film deleted");
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
