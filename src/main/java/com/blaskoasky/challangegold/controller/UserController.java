package com.blaskoasky.challangegold.controller;

import com.blaskoasky.challangegold.mapper.user.UpdateUserRequestDTO;
import com.blaskoasky.challangegold.mapper.user.UserMapper;
import com.blaskoasky.challangegold.model.user.UserEntity;
import com.blaskoasky.challangegold.service.InvoiceService;
import com.blaskoasky.challangegold.service.user.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bioskop/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private InvoiceService invoiceService;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    //          ----> GET MAPPING <----
    @Operation(summary = "counting users in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Counted!",
                    content = {@Content(schema = @Schema(example = "Counted!"))})
    })
    @GetMapping("/public/count-users")
    public ResponseEntity<Integer> countUsers() {
        Integer jumlahUsers = userService.countUsers();
        LOG.info("Jumlah users: {}", jumlahUsers);
        return ResponseEntity.ok(jumlahUsers);
    }

    @Operation(summary = "show all users data in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Users found!",
                    content = {@Content(schema = @Schema(example = "Users found!"))})
    })
    @GetMapping("/admin/show-users")
    public ResponseEntity<List<UserEntity>> showAllUsers() {
        List<UserEntity> allUsers = userService.showAllUsers();
        LOG.info("Users found: {}", allUsers.size());
        return ResponseEntity.ok(allUsers);
    }

    @Operation(summary = "show a user by their userId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found!",
                    content = {@Content(schema = @Schema(example = "User found!"))})
    })
    @GetMapping("/public/get-user/{id}")
    public ResponseEntity<UserEntity> getUserById(@PathVariable("id") Integer userId) {
        UserEntity user = userService.getUserById(userId);
        LOG.info("User found: {}", user.getUsername());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    //          ----> UPDATE MAPPING <----
    @Operation(summary = "Change a user data, update 'username', 'email', and 'password' related to their userId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated!",
                    content = {@Content(schema = @Schema(example = "User updated!"))})
    })
    @PutMapping("/customer/update-user")
    public ResponseEntity<HttpStatus> updateUser(@RequestBody UpdateUserRequestDTO updatedUser) {
        userService.updateUser(userMapper.mapUpdateUser(updatedUser));
        LOG.info("User updated: {}", updatedUser.getUsername());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //          ----> DELETE MAPPING <----
    @Operation(summary = "Delete a user by their userId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "User deleted!",
                    content = {@Content(schema = @Schema(example = "User deleted!"))})
    })
    @DeleteMapping("/customer/delete-user/{id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") Integer userId) {
        userService.deleteUser(userId);
        LOG.info("User id deleted: {}", userId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
