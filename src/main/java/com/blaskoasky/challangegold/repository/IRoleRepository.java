package com.blaskoasky.challangegold.repository;

import com.blaskoasky.challangegold.enume.ERole;
import com.blaskoasky.challangegold.model.user.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRoleRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(ERole name);
}
