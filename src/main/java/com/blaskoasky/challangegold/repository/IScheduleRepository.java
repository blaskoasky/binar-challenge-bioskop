package com.blaskoasky.challangegold.repository;

import com.blaskoasky.challangegold.model.film.ScheduleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface IScheduleRepository extends JpaRepository<ScheduleEntity, Integer> {

    @Modifying
    @Query(nativeQuery = true, value = "select * from schedules where film_id = :film_id")
    List<ScheduleEntity> scheduleEntity(
            @Param("film_id") Integer filmId
    );
}
