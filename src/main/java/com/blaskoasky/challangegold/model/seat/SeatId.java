package com.blaskoasky.challangegold.model.seat;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class SeatId implements Serializable {
    private Integer studioName;
    private String nomorKursi;
}
