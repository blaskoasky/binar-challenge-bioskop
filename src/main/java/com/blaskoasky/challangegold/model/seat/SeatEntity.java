package com.blaskoasky.challangegold.model.seat;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity(name = "seats")
@Getter
@Setter
public class SeatEntity {
    @EmbeddedId
    private SeatId seatId;
}
