package com.blaskoasky.challangegold.model.film;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "films")
public class FilmEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "film_id")
    private Integer filmId;

    @Column(name = "film_code")
    private String filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "sedang_tayang")
    private Integer sedangTayang;

    public FilmEntity() {

    }

    public FilmEntity(Integer filmId) {
        this.filmId = filmId;
    }
}
