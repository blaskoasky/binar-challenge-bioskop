package com.blaskoasky.challangegold.model.film;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "schedules")
@Getter
@Setter
public class ScheduleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @ManyToOne(targetEntity = FilmEntity.class) //cascade = CascadeType.DETACH)
    @JoinColumn(name = "film_id")
    private FilmEntity filmId;

    @Column(name = "tanggal_tayang")
    private String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;

    public ScheduleEntity() {
        // Constructor
    }

}
