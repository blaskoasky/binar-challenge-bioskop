package com.blaskoasky.challangegold.model.film;

public interface IFilmScheduleDTO {
    String getFilmCode();

    String getFilmName();

    Integer getSedangTayang();

    String getTanggalTayang();

    String getJamMulai();

    String getJamSelesai();

    Integer getHargaTiket();
}
