package com.blaskoasky.challangegold.mapper.film;

import com.blaskoasky.challangegold.model.film.FilmEntity;
import org.springframework.stereotype.Component;

@Component
public class FilmMapper {
    public FilmEntity mapAddFilm(AddFilmRequestDTO request) {
        FilmEntity film = new FilmEntity();
        film.setFilmCode(request.getFilmCode());
        film.setFilmName(request.getFilmName());
        film.setSedangTayang(request.getSedangTayang());
        return film;
    }

    public FilmEntity mapUpdateFilm(UpdateFilmRequestDTO request) {
        FilmEntity film = new FilmEntity();
        film.setFilmId(request.getFilmId());
        film.setFilmCode(request.getFilmCode());
        film.setFilmName(request.getFilmName());
        film.setSedangTayang(request.getSedangTayang());
        return film;
    }
}
