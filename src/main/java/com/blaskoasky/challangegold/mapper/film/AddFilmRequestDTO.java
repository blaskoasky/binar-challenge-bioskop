package com.blaskoasky.challangegold.mapper.film;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AddFilmRequestDTO {
    private String filmCode;
    private String filmName;
    private Integer sedangTayang;
}
