package com.blaskoasky.challangegold.mapper.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AddUserRequestDTO {
    private String username;
    private String email;
    private String password;
}
