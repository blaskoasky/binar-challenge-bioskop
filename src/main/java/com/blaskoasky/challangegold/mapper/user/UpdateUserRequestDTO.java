package com.blaskoasky.challangegold.mapper.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateUserRequestDTO {
    private String username;
    private String email;
    private String password;
    private Integer userId;
}
