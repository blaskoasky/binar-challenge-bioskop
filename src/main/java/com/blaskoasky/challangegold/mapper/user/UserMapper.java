package com.blaskoasky.challangegold.mapper.user;

import com.blaskoasky.challangegold.model.user.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserEntity mapAddUser(AddUserRequestDTO request) {
        UserEntity user = new UserEntity();
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        return user;
    }

    public UserEntity mapUpdateUser(UpdateUserRequestDTO request) {
        UserEntity user = new UserEntity();
        user.setUserId(request.getUserId());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        return user;
    }
}
