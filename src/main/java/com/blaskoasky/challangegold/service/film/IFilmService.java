package com.blaskoasky.challangegold.service.film;

import com.blaskoasky.challangegold.model.film.FilmEntity;
import com.blaskoasky.challangegold.model.film.IFilmScheduleDTO;
import com.blaskoasky.challangegold.model.film.ScheduleEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IFilmService {

    Integer countFilms();

    void addFilm(FilmEntity film);

    void updateFilm(FilmEntity film);

    void deleteFilm(Integer filmId);

    void deleteScheduleFilm(Integer filmId);

    List<FilmEntity> showAllFilms();

    List<IFilmScheduleDTO> showScheduleFilmNew(Integer filmId);

    FilmEntity getFilmById(Integer userId);

    List<ScheduleEntity> scheduleFilm(Integer filmId);
}
