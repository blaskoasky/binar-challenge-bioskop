package com.blaskoasky.challangegold.service.film;

import com.blaskoasky.challangegold.model.film.FilmEntity;
import com.blaskoasky.challangegold.model.film.IFilmScheduleDTO;
import com.blaskoasky.challangegold.model.film.ScheduleEntity;
import com.blaskoasky.challangegold.repository.IFilmRepository;
import com.blaskoasky.challangegold.repository.IScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements IFilmService {

    @Autowired
    public FilmServiceImpl(IFilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Autowired
    private final IFilmRepository filmRepository;

    @Autowired
    private IScheduleRepository scheduleRepository;


    @Override
    public Integer countFilms() {
        return filmRepository.countFilms();
    }

    @Override
    public void addFilm(FilmEntity film) {
        String filmCode = film.getFilmCode();
        String filmName = film.getFilmName();
        Integer sedangTayang = film.getSedangTayang();
        filmRepository.addFilm(filmCode, filmName, sedangTayang);

    }

    @Override
    public void updateFilm(FilmEntity film) {
        Integer filmId = film.getFilmId();
        String filmCode = film.getFilmCode();
        String filmName = film.getFilmName();
        Integer sedangTayang = film.getSedangTayang();
        filmRepository.updateFilm(filmCode, filmName, sedangTayang, filmId);
    }

    @Override
    public void deleteFilm(Integer filmId) {
        filmRepository.deleteFilm(filmId);
    }

    @Override
    public void deleteScheduleFilm(Integer filmId) {
        filmRepository.deleteScheduleFilm(filmId);
    }

    @Cacheable(value = "showAllFilms")
    @Override
    public List<FilmEntity> showAllFilms() {
        return filmRepository.findAll();
    }

    @Override
    public List<IFilmScheduleDTO> showScheduleFilmNew(Integer filmId) {
        return filmRepository.showFilmScheduleNew(filmId);
    }

    @Cacheable(value = "getFilmById")
    @Override
    public FilmEntity getFilmById(Integer userId) {
        return filmRepository.getById(userId);
    }

    @Cacheable(value = "scheduleFilm")
    @Override
    public List<ScheduleEntity> scheduleFilm(Integer filmId) {
        return scheduleRepository.scheduleEntity(filmId);
    }

}
