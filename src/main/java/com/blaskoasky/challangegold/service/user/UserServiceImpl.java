package com.blaskoasky.challangegold.service.user;

import com.blaskoasky.challangegold.model.user.UserEntity;
import com.blaskoasky.challangegold.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    public UserServiceImpl(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private final IUserRepository userRepository;

    @Override
    public Integer countUsers() {
        return userRepository.countUsers();
    }

    @Override
    public void updateUser(UserEntity user) {
        Integer userId = user.getUserId();
        String username = user.getUsername();
        String email = user.getEmail();
        String password = user.getPassword();
        userRepository.updateUser(username, email, password, userId);
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteUser(userId);
    }

    @Override
    public List<UserEntity> showAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserEntity getUserById(Integer userId) {
        return userRepository.getUserById(userId);
    }
}
