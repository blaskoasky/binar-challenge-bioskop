package com.blaskoasky.challangegold.service.user;

import com.blaskoasky.challangegold.model.user.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IUserService {

    Integer countUsers();

    void updateUser(UserEntity user);

    void deleteUser(Integer userId);

    List<UserEntity> showAllUsers();

    UserEntity getUserById(Integer userId);
}
