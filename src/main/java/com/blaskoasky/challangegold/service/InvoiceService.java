package com.blaskoasky.challangegold.service;

import com.blaskoasky.challangegold.model.film.ScheduleEntity;
import com.blaskoasky.challangegold.model.user.UserEntity;
import com.blaskoasky.challangegold.service.film.IFilmService;
import com.blaskoasky.challangegold.service.user.IUserService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InvoiceService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IFilmService filmService;

    @Autowired
    private HttpServletResponse hResponse;

    public void getInvoice(Integer userId, Integer filmId) throws IOException, JRException {

        JasperPrint jasperPrint = JasperFillManager.fillReport(giveJasperReport(), giveParameter(), giveBeanCollection(userId, filmId));
        servletResponse();
        JasperExportManager.exportReportToPdfStream(jasperPrint, hResponse.getOutputStream());
    }

    private void servletResponse() {
        hResponse.setContentType("application/pdf");
        hResponse.addHeader("Content-Disposition", "inline; filename=kartu.pdf;");
    }

    // Ganti DATA DIMARI
    private List<Map<String, String>> giveDataList(Integer userId, Integer filmId) {
        List<Map<String, String>> dataList = new ArrayList<>();
        Map<String, String> data = new HashMap<>();

        UserEntity user = userService.getUserById(userId);
        List<ScheduleEntity> scheduleL = filmService.scheduleFilm(filmId);
        ScheduleEntity schedule = scheduleL.get(0);

        data.put("username", user.getUsername());
        data.put("nomorKursi", "A1*"); // HARDCODED
        data.put("filmName", schedule.getFilmId().getFilmName());
        data.put("tanggalTayang", schedule.getTanggalTayang());
        data.put("jamMulai", schedule.getJamMulai());
        data.put("jamSelesai", schedule.getJamSelesai());
        data.put("hargaTiket", schedule.getHargaTiket().toString());

        dataList.add(data);
        return dataList;
    }

    private JasperReport giveJasperReport() throws IOException, JRException {
        return JasperCompileManager.compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "invoiceTiket.jrxml").getAbsolutePath());
    }

    private HashMap<String, Object> giveParameter() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("createdBy", "iriantomauduta");
        return param;
    }

    private JRBeanCollectionDataSource giveBeanCollection(Integer userId, Integer filmId) {
        return new JRBeanCollectionDataSource(giveDataList(userId, filmId));
    }

}
