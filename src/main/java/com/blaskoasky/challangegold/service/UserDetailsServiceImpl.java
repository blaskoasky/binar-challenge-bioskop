package com.blaskoasky.challangegold.service;

import com.blaskoasky.challangegold.model.user.UserDetailsImpl;
import com.blaskoasky.challangegold.model.user.UserEntity;
import com.blaskoasky.challangegold.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    IUserRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = usersRepository.findByUsername(username);
        return UserDetailsImpl.build(user);
    }
}
