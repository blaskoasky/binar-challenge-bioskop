package com.blaskoasky.challangegold.service.user;

import com.blaskoasky.challangegold.model.user.UserEntity;
import com.blaskoasky.challangegold.repository.IUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IUserServiceMockTest {

    @Mock
    private IUserRepository userRepository;

    private IUserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.userService = new UserServiceImpl(userRepository);
    }

    @Test
    void countUsers() {
        Assertions.assertEquals(userRepository.countUsers(), userService.countUsers());
    }

    @Test
    void updateUser() {
        UserEntity updatedUser = new UserEntity();
        Assertions.assertDoesNotThrow(() -> userService.updateUser(updatedUser));
    }

    @Test
    void deleteUser() {
        Assertions.assertDoesNotThrow(() -> userService.deleteUser(1));
    }

    @Test
    void showAllUsers() {
        Assertions.assertEquals(userRepository.findAll(), userService.showAllUsers());
    }

    @Test
    void getUserById() {
        Assertions.assertEquals(userRepository.getUserById(1), userService.getUserById(1));
    }
}