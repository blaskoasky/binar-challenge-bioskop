package com.blaskoasky.challangegold.service.film;

import com.blaskoasky.challangegold.model.film.FilmEntity;
import com.blaskoasky.challangegold.repository.IFilmRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IFilmServiceMockTest {

    @Mock
    private IFilmRepository filmRepository;

    private IFilmService filmService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.filmService = new FilmServiceImpl(filmRepository);
    }

    @Test
    void countFilms() {
        Assertions.assertEquals(filmRepository.countFilms(), filmService.countFilms());
    }

    @Test
    void addFilm() {
        FilmEntity film = new FilmEntity();
        Assertions.assertDoesNotThrow(() -> filmService.addFilm(film));
    }

    @Test
    void updateFilm() {
        FilmEntity updatedFilm = new FilmEntity();
        Assertions.assertDoesNotThrow(() -> filmService.updateFilm(updatedFilm));
    }

    @Test
    void deleteFilm() {
        Assertions.assertDoesNotThrow(() -> filmService.deleteFilm(1));
    }

    @Test
    void deleteScheduleFilm() {
        Assertions.assertDoesNotThrow(() -> filmService.deleteScheduleFilm(1));
    }

    @Test
    void showAllFilms() {
        Assertions.assertEquals(filmRepository.findAll(), filmService.showAllFilms());
    }

    @Test
    void showScheduleFilmNew() {
        Assertions.assertEquals(filmRepository.showFilmScheduleNew(1), filmService.showScheduleFilmNew(1));
    }
}