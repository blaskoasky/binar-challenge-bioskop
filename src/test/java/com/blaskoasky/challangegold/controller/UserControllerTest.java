package com.blaskoasky.challangegold.controller;

import com.blaskoasky.challangegold.mapper.user.UpdateUserRequestDTO;
import com.blaskoasky.challangegold.repository.IUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class UserControllerTest {

    @Autowired
    private UserController userController;

    @Autowired
    private IUserRepository userRepository;

    @Test
    void countUsers() {
        Integer user = userRepository.countUsers();
        Assertions.assertEquals(new ResponseEntity<>(user, HttpStatus.OK), userController.countUsers());
        userController.countUsers();
    }

    @Test
    @DisplayName("7. Mengupdate data User")
    void updateUser() {
        UpdateUserRequestDTO updatedUser = new UpdateUserRequestDTO("apdetlah", "apdetnih@email.com", "apdetpassword", 10);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), userController.updateUser(updatedUser));
    }

    @Test
    @DisplayName("8. Menghapus user")
    void deleteUser() {
        int userId = 10;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), userController.deleteUser(userId));
    }
}