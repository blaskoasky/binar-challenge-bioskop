package com.blaskoasky.challangegold.controller;

import com.blaskoasky.challangegold.mapper.film.AddFilmRequestDTO;
import com.blaskoasky.challangegold.mapper.film.UpdateFilmRequestDTO;
import com.blaskoasky.challangegold.model.film.FilmEntity;
import com.blaskoasky.challangegold.model.film.IFilmScheduleDTO;
import com.blaskoasky.challangegold.repository.IFilmRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
class FilmControllerTest {

    @Autowired
    private FilmController filmController;

    @Autowired
    private IFilmRepository filmRepository;

    // Hitung berapa jumlah films
    @Test
    @DisplayName("0. Hitung Jumlah Data")
    void countFilms() {
        Integer films = filmRepository.countFilms();
        Assertions.assertEquals(new ResponseEntity<>(films, HttpStatus.OK), filmController.countFilms());
    }

    // Tambah film
    @Test
    @DisplayName("1. Menambah Film Baru")
    void addFilm() {
        AddFilmRequestDTO newFilm = new AddFilmRequestDTO("filmKodeBaruNich1", "filmBaruNich1", 1);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.CREATED), filmController.addFilm(newFilm));
    }

    // Update info film
    @Test
    @DisplayName("2. Update Data Film")
    void updateFilm() {
        UpdateFilmRequestDTO updatedFilm = new UpdateFilmRequestDTO("ufilmKodeBaruNich1", "filmBaruNich1", 1, 18);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), filmController.updateFilm(updatedFilm));

    }

    // Menampilkan show yang sedang tayang
    @Test
    @DisplayName("4. Menampilkan Film-film yang sedang tayang")
    void showSedangTayang() {
        // List<FilmEntity> listFilms = filmRepository.findAll();
        // Assertions.assertEquals(new ResponseEntity<>(listFilms, HttpStatus.OK), filmController.showAllSedangTayang());
        Assertions.assertDoesNotThrow(() -> filmController.showAllSedangTayang());
    }

    // Menampilkan schedule suatu film
    @Test
    @DisplayName("5. Menampilkan Jadwal suatu film (by id)")
    void showFilmSchedule() {
        int filmId = 11;
        List<IFilmScheduleDTO> schedule = filmRepository.showFilmScheduleNew(filmId);
        filmController.showScheduleFilm(11);
        Assertions.assertEquals(new ResponseEntity<>(schedule, HttpStatus.OK), filmController.showScheduleFilm(filmId));
    }

    // Hapus film (plus schedule)
    @Test
    @DisplayName("3. Menghapus film (+schedulefilm)")
    void deleteFilm() {
        int filmDeleted = 13;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), filmController.deleteFilm(filmDeleted));
    }
}